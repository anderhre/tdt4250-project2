/**
 */
package tdt4250.sp;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see tdt4250.sp.SpFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore validationDelegates='http://www.eclipse.org/acceleo/query/1.0'"
 * @generated
 */
public interface SpPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "sp";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "platform:/plugin/tdt4250.sp.model/model/studyPlan.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "sp";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	SpPackage eINSTANCE = tdt4250.sp.impl.SpPackageImpl.init();

	/**
	 * The meta object id for the '{@link tdt4250.sp.impl.SemesterImpl <em>Semester</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.sp.impl.SemesterImpl
	 * @see tdt4250.sp.impl.SpPackageImpl#getSemester()
	 * @generated
	 */
	int SEMESTER = 2;

	/**
	 * The meta object id for the '{@link tdt4250.sp.impl.CourseImpl <em>Course</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.sp.impl.CourseImpl
	 * @see tdt4250.sp.impl.SpPackageImpl#getCourse()
	 * @generated
	 */
	int COURSE = 5;

	/**
	 * The meta object id for the '{@link tdt4250.sp.impl.DepartmentImpl <em>Department</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.sp.impl.DepartmentImpl
	 * @see tdt4250.sp.impl.SpPackageImpl#getDepartment()
	 * @generated
	 */
	int DEPARTMENT = 0;

	/**
	 * The feature id for the '<em><b>Courses</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT__COURSES = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT__NAME = 1;

	/**
	 * The feature id for the '<em><b>Programmes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT__PROGRAMMES = 2;

	/**
	 * The feature id for the '<em><b>Abbreviation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT__ABBREVIATION = 3;

	/**
	 * The number of structural features of the '<em>Department</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT_FEATURE_COUNT = 4;

	/**
	 * The operation id for the '<em>Add Course</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT___ADD_COURSE__COURSE = 0;

	/**
	 * The number of operations of the '<em>Department</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link tdt4250.sp.impl.StudyProgrammeImpl <em>Study Programme</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.sp.impl.StudyProgrammeImpl
	 * @see tdt4250.sp.impl.SpPackageImpl#getStudyProgramme()
	 * @generated
	 */
	int STUDY_PROGRAMME = 1;

	/**
	 * The feature id for the '<em><b>Semesters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY_PROGRAMME__SEMESTERS = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY_PROGRAMME__NAME = 1;

	/**
	 * The number of structural features of the '<em>Study Programme</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY_PROGRAMME_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Study Programme</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDY_PROGRAMME_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Year</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEMESTER__YEAR = 0;

	/**
	 * The feature id for the '<em><b>Term</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEMESTER__TERM = 1;

	/**
	 * The feature id for the '<em><b>Specialization</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEMESTER__SPECIALIZATION = 2;

	/**
	 * The feature id for the '<em><b>Mandatory Course Group</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEMESTER__MANDATORY_COURSE_GROUP = 3;

	/**
	 * The feature id for the '<em><b>Elective Course Group</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEMESTER__ELECTIVE_COURSE_GROUP = 4;

	/**
	 * The number of structural features of the '<em>Semester</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEMESTER_FEATURE_COUNT = 5;

	/**
	 * The operation id for the '<em>Add Course</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEMESTER___ADD_COURSE__COURSE_STATUS = 0;

	/**
	 * The operation id for the '<em>Choose Specialization</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEMESTER___CHOOSE_SPECIALIZATION__SPECIALIZATION = 1;

	/**
	 * The number of operations of the '<em>Semester</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEMESTER_OPERATION_COUNT = 2;

	/**
	 * The meta object id for the '{@link tdt4250.sp.impl.MandatoryCourseGroupImpl <em>Mandatory Course Group</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.sp.impl.MandatoryCourseGroupImpl
	 * @see tdt4250.sp.impl.SpPackageImpl#getMandatoryCourseGroup()
	 * @generated
	 */
	int MANDATORY_COURSE_GROUP = 3;

	/**
	 * The feature id for the '<em><b>O</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MANDATORY_COURSE_GROUP__O = 0;

	/**
	 * The feature id for the '<em><b>M1a</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MANDATORY_COURSE_GROUP__M1A = 1;

	/**
	 * The feature id for the '<em><b>M2a</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MANDATORY_COURSE_GROUP__M2A = 2;

	/**
	 * The number of structural features of the '<em>Mandatory Course Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MANDATORY_COURSE_GROUP_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Mandatory Course Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MANDATORY_COURSE_GROUP_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250.sp.impl.ElectiveCourseGroupImpl <em>Elective Course Group</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.sp.impl.ElectiveCourseGroupImpl
	 * @see tdt4250.sp.impl.SpPackageImpl#getElectiveCourseGroup()
	 * @generated
	 */
	int ELECTIVE_COURSE_GROUP = 4;

	/**
	 * The feature id for the '<em><b>Va</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELECTIVE_COURSE_GROUP__VA = 0;

	/**
	 * The feature id for the '<em><b>Vb</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELECTIVE_COURSE_GROUP__VB = 1;

	/**
	 * The number of structural features of the '<em>Elective Course Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELECTIVE_COURSE_GROUP_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Elective Course Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELECTIVE_COURSE_GROUP_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__CODE = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__NAME = 1;

	/**
	 * The feature id for the '<em><b>Study Points</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__STUDY_POINTS = 2;

	/**
	 * The feature id for the '<em><b>Owned By</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__OWNED_BY = 3;

	/**
	 * The feature id for the '<em><b>Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__LEVEL = 4;

	/**
	 * The number of structural features of the '<em>Course</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Course</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250.sp.Specialization <em>Specialization</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.sp.Specialization
	 * @see tdt4250.sp.impl.SpPackageImpl#getSpecialization()
	 * @generated
	 */
	int SPECIALIZATION = 6;


	/**
	 * The meta object id for the '{@link tdt4250.sp.Term <em>Term</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.sp.Term
	 * @see tdt4250.sp.impl.SpPackageImpl#getTerm()
	 * @generated
	 */
	int TERM = 7;


	/**
	 * The meta object id for the '{@link tdt4250.sp.CourseLevel <em>Course Level</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.sp.CourseLevel
	 * @see tdt4250.sp.impl.SpPackageImpl#getCourseLevel()
	 * @generated
	 */
	int COURSE_LEVEL = 8;


	/**
	 * The meta object id for the '{@link tdt4250.sp.Status <em>Status</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.sp.Status
	 * @see tdt4250.sp.impl.SpPackageImpl#getStatus()
	 * @generated
	 */
	int STATUS = 9;


	/**
	 * Returns the meta object for class '{@link tdt4250.sp.Semester <em>Semester</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Semester</em>'.
	 * @see tdt4250.sp.Semester
	 * @generated
	 */
	EClass getSemester();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.sp.Semester#getYear <em>Year</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Year</em>'.
	 * @see tdt4250.sp.Semester#getYear()
	 * @see #getSemester()
	 * @generated
	 */
	EAttribute getSemester_Year();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.sp.Semester#getTerm <em>Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Term</em>'.
	 * @see tdt4250.sp.Semester#getTerm()
	 * @see #getSemester()
	 * @generated
	 */
	EAttribute getSemester_Term();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.sp.Semester#getSpecialization <em>Specialization</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Specialization</em>'.
	 * @see tdt4250.sp.Semester#getSpecialization()
	 * @see #getSemester()
	 * @generated
	 */
	EAttribute getSemester_Specialization();

	/**
	 * Returns the meta object for the containment reference '{@link tdt4250.sp.Semester#getMandatoryCourseGroup <em>Mandatory Course Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Mandatory Course Group</em>'.
	 * @see tdt4250.sp.Semester#getMandatoryCourseGroup()
	 * @see #getSemester()
	 * @generated
	 */
	EReference getSemester_MandatoryCourseGroup();

	/**
	 * Returns the meta object for the containment reference '{@link tdt4250.sp.Semester#getElectiveCourseGroup <em>Elective Course Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Elective Course Group</em>'.
	 * @see tdt4250.sp.Semester#getElectiveCourseGroup()
	 * @see #getSemester()
	 * @generated
	 */
	EReference getSemester_ElectiveCourseGroup();

	/**
	 * Returns the meta object for the '{@link tdt4250.sp.Semester#addCourse(tdt4250.sp.Course, tdt4250.sp.Status) <em>Add Course</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Course</em>' operation.
	 * @see tdt4250.sp.Semester#addCourse(tdt4250.sp.Course, tdt4250.sp.Status)
	 * @generated
	 */
	EOperation getSemester__AddCourse__Course_Status();

	/**
	 * Returns the meta object for the '{@link tdt4250.sp.Semester#chooseSpecialization(tdt4250.sp.Specialization) <em>Choose Specialization</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Choose Specialization</em>' operation.
	 * @see tdt4250.sp.Semester#chooseSpecialization(tdt4250.sp.Specialization)
	 * @generated
	 */
	EOperation getSemester__ChooseSpecialization__Specialization();

	/**
	 * Returns the meta object for class '{@link tdt4250.sp.MandatoryCourseGroup <em>Mandatory Course Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Mandatory Course Group</em>'.
	 * @see tdt4250.sp.MandatoryCourseGroup
	 * @generated
	 */
	EClass getMandatoryCourseGroup();

	/**
	 * Returns the meta object for the reference list '{@link tdt4250.sp.MandatoryCourseGroup#getO <em>O</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>O</em>'.
	 * @see tdt4250.sp.MandatoryCourseGroup#getO()
	 * @see #getMandatoryCourseGroup()
	 * @generated
	 */
	EReference getMandatoryCourseGroup_O();

	/**
	 * Returns the meta object for the reference list '{@link tdt4250.sp.MandatoryCourseGroup#getM1a <em>M1a</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>M1a</em>'.
	 * @see tdt4250.sp.MandatoryCourseGroup#getM1a()
	 * @see #getMandatoryCourseGroup()
	 * @generated
	 */
	EReference getMandatoryCourseGroup_M1a();

	/**
	 * Returns the meta object for the reference list '{@link tdt4250.sp.MandatoryCourseGroup#getM2a <em>M2a</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>M2a</em>'.
	 * @see tdt4250.sp.MandatoryCourseGroup#getM2a()
	 * @see #getMandatoryCourseGroup()
	 * @generated
	 */
	EReference getMandatoryCourseGroup_M2a();

	/**
	 * Returns the meta object for class '{@link tdt4250.sp.ElectiveCourseGroup <em>Elective Course Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Elective Course Group</em>'.
	 * @see tdt4250.sp.ElectiveCourseGroup
	 * @generated
	 */
	EClass getElectiveCourseGroup();

	/**
	 * Returns the meta object for the reference list '{@link tdt4250.sp.ElectiveCourseGroup#getVa <em>Va</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Va</em>'.
	 * @see tdt4250.sp.ElectiveCourseGroup#getVa()
	 * @see #getElectiveCourseGroup()
	 * @generated
	 */
	EReference getElectiveCourseGroup_Va();

	/**
	 * Returns the meta object for the reference list '{@link tdt4250.sp.ElectiveCourseGroup#getVb <em>Vb</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Vb</em>'.
	 * @see tdt4250.sp.ElectiveCourseGroup#getVb()
	 * @see #getElectiveCourseGroup()
	 * @generated
	 */
	EReference getElectiveCourseGroup_Vb();

	/**
	 * Returns the meta object for class '{@link tdt4250.sp.Course <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Course</em>'.
	 * @see tdt4250.sp.Course
	 * @generated
	 */
	EClass getCourse();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.sp.Course#getCode <em>Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Code</em>'.
	 * @see tdt4250.sp.Course#getCode()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Code();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.sp.Course#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see tdt4250.sp.Course#getName()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Name();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.sp.Course#getStudyPoints <em>Study Points</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Study Points</em>'.
	 * @see tdt4250.sp.Course#getStudyPoints()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_StudyPoints();

	/**
	 * Returns the meta object for the container reference '{@link tdt4250.sp.Course#getOwnedBy <em>Owned By</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Owned By</em>'.
	 * @see tdt4250.sp.Course#getOwnedBy()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_OwnedBy();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.sp.Course#getLevel <em>Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Level</em>'.
	 * @see tdt4250.sp.Course#getLevel()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Level();

	/**
	 * Returns the meta object for class '{@link tdt4250.sp.Department <em>Department</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Department</em>'.
	 * @see tdt4250.sp.Department
	 * @generated
	 */
	EClass getDepartment();

	/**
	 * Returns the meta object for the containment reference list '{@link tdt4250.sp.Department#getCourses <em>Courses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Courses</em>'.
	 * @see tdt4250.sp.Department#getCourses()
	 * @see #getDepartment()
	 * @generated
	 */
	EReference getDepartment_Courses();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.sp.Department#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see tdt4250.sp.Department#getName()
	 * @see #getDepartment()
	 * @generated
	 */
	EAttribute getDepartment_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link tdt4250.sp.Department#getProgrammes <em>Programmes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Programmes</em>'.
	 * @see tdt4250.sp.Department#getProgrammes()
	 * @see #getDepartment()
	 * @generated
	 */
	EReference getDepartment_Programmes();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.sp.Department#getAbbreviation <em>Abbreviation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Abbreviation</em>'.
	 * @see tdt4250.sp.Department#getAbbreviation()
	 * @see #getDepartment()
	 * @generated
	 */
	EAttribute getDepartment_Abbreviation();

	/**
	 * Returns the meta object for the '{@link tdt4250.sp.Department#addCourse(tdt4250.sp.Course) <em>Add Course</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Course</em>' operation.
	 * @see tdt4250.sp.Department#addCourse(tdt4250.sp.Course)
	 * @generated
	 */
	EOperation getDepartment__AddCourse__Course();

	/**
	 * Returns the meta object for class '{@link tdt4250.sp.StudyProgramme <em>Study Programme</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Study Programme</em>'.
	 * @see tdt4250.sp.StudyProgramme
	 * @generated
	 */
	EClass getStudyProgramme();

	/**
	 * Returns the meta object for the containment reference list '{@link tdt4250.sp.StudyProgramme#getSemesters <em>Semesters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Semesters</em>'.
	 * @see tdt4250.sp.StudyProgramme#getSemesters()
	 * @see #getStudyProgramme()
	 * @generated
	 */
	EReference getStudyProgramme_Semesters();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.sp.StudyProgramme#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see tdt4250.sp.StudyProgramme#getName()
	 * @see #getStudyProgramme()
	 * @generated
	 */
	EAttribute getStudyProgramme_Name();

	/**
	 * Returns the meta object for enum '{@link tdt4250.sp.Specialization <em>Specialization</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Specialization</em>'.
	 * @see tdt4250.sp.Specialization
	 * @generated
	 */
	EEnum getSpecialization();

	/**
	 * Returns the meta object for enum '{@link tdt4250.sp.Term <em>Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Term</em>'.
	 * @see tdt4250.sp.Term
	 * @generated
	 */
	EEnum getTerm();

	/**
	 * Returns the meta object for enum '{@link tdt4250.sp.CourseLevel <em>Course Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Course Level</em>'.
	 * @see tdt4250.sp.CourseLevel
	 * @generated
	 */
	EEnum getCourseLevel();

	/**
	 * Returns the meta object for enum '{@link tdt4250.sp.Status <em>Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Status</em>'.
	 * @see tdt4250.sp.Status
	 * @generated
	 */
	EEnum getStatus();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	SpFactory getSpFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link tdt4250.sp.impl.SemesterImpl <em>Semester</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.sp.impl.SemesterImpl
		 * @see tdt4250.sp.impl.SpPackageImpl#getSemester()
		 * @generated
		 */
		EClass SEMESTER = eINSTANCE.getSemester();

		/**
		 * The meta object literal for the '<em><b>Year</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SEMESTER__YEAR = eINSTANCE.getSemester_Year();

		/**
		 * The meta object literal for the '<em><b>Term</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SEMESTER__TERM = eINSTANCE.getSemester_Term();

		/**
		 * The meta object literal for the '<em><b>Specialization</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SEMESTER__SPECIALIZATION = eINSTANCE.getSemester_Specialization();

		/**
		 * The meta object literal for the '<em><b>Mandatory Course Group</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEMESTER__MANDATORY_COURSE_GROUP = eINSTANCE.getSemester_MandatoryCourseGroup();

		/**
		 * The meta object literal for the '<em><b>Elective Course Group</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEMESTER__ELECTIVE_COURSE_GROUP = eINSTANCE.getSemester_ElectiveCourseGroup();

		/**
		 * The meta object literal for the '<em><b>Add Course</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SEMESTER___ADD_COURSE__COURSE_STATUS = eINSTANCE.getSemester__AddCourse__Course_Status();

		/**
		 * The meta object literal for the '<em><b>Choose Specialization</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SEMESTER___CHOOSE_SPECIALIZATION__SPECIALIZATION = eINSTANCE.getSemester__ChooseSpecialization__Specialization();

		/**
		 * The meta object literal for the '{@link tdt4250.sp.impl.MandatoryCourseGroupImpl <em>Mandatory Course Group</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.sp.impl.MandatoryCourseGroupImpl
		 * @see tdt4250.sp.impl.SpPackageImpl#getMandatoryCourseGroup()
		 * @generated
		 */
		EClass MANDATORY_COURSE_GROUP = eINSTANCE.getMandatoryCourseGroup();

		/**
		 * The meta object literal for the '<em><b>O</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MANDATORY_COURSE_GROUP__O = eINSTANCE.getMandatoryCourseGroup_O();

		/**
		 * The meta object literal for the '<em><b>M1a</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MANDATORY_COURSE_GROUP__M1A = eINSTANCE.getMandatoryCourseGroup_M1a();

		/**
		 * The meta object literal for the '<em><b>M2a</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MANDATORY_COURSE_GROUP__M2A = eINSTANCE.getMandatoryCourseGroup_M2a();

		/**
		 * The meta object literal for the '{@link tdt4250.sp.impl.ElectiveCourseGroupImpl <em>Elective Course Group</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.sp.impl.ElectiveCourseGroupImpl
		 * @see tdt4250.sp.impl.SpPackageImpl#getElectiveCourseGroup()
		 * @generated
		 */
		EClass ELECTIVE_COURSE_GROUP = eINSTANCE.getElectiveCourseGroup();

		/**
		 * The meta object literal for the '<em><b>Va</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELECTIVE_COURSE_GROUP__VA = eINSTANCE.getElectiveCourseGroup_Va();

		/**
		 * The meta object literal for the '<em><b>Vb</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELECTIVE_COURSE_GROUP__VB = eINSTANCE.getElectiveCourseGroup_Vb();

		/**
		 * The meta object literal for the '{@link tdt4250.sp.impl.CourseImpl <em>Course</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.sp.impl.CourseImpl
		 * @see tdt4250.sp.impl.SpPackageImpl#getCourse()
		 * @generated
		 */
		EClass COURSE = eINSTANCE.getCourse();

		/**
		 * The meta object literal for the '<em><b>Code</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__CODE = eINSTANCE.getCourse_Code();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__NAME = eINSTANCE.getCourse_Name();

		/**
		 * The meta object literal for the '<em><b>Study Points</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__STUDY_POINTS = eINSTANCE.getCourse_StudyPoints();

		/**
		 * The meta object literal for the '<em><b>Owned By</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__OWNED_BY = eINSTANCE.getCourse_OwnedBy();

		/**
		 * The meta object literal for the '<em><b>Level</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__LEVEL = eINSTANCE.getCourse_Level();

		/**
		 * The meta object literal for the '{@link tdt4250.sp.impl.DepartmentImpl <em>Department</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.sp.impl.DepartmentImpl
		 * @see tdt4250.sp.impl.SpPackageImpl#getDepartment()
		 * @generated
		 */
		EClass DEPARTMENT = eINSTANCE.getDepartment();

		/**
		 * The meta object literal for the '<em><b>Courses</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPARTMENT__COURSES = eINSTANCE.getDepartment_Courses();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEPARTMENT__NAME = eINSTANCE.getDepartment_Name();

		/**
		 * The meta object literal for the '<em><b>Programmes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPARTMENT__PROGRAMMES = eINSTANCE.getDepartment_Programmes();

		/**
		 * The meta object literal for the '<em><b>Abbreviation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEPARTMENT__ABBREVIATION = eINSTANCE.getDepartment_Abbreviation();

		/**
		 * The meta object literal for the '<em><b>Add Course</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation DEPARTMENT___ADD_COURSE__COURSE = eINSTANCE.getDepartment__AddCourse__Course();

		/**
		 * The meta object literal for the '{@link tdt4250.sp.impl.StudyProgrammeImpl <em>Study Programme</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.sp.impl.StudyProgrammeImpl
		 * @see tdt4250.sp.impl.SpPackageImpl#getStudyProgramme()
		 * @generated
		 */
		EClass STUDY_PROGRAMME = eINSTANCE.getStudyProgramme();

		/**
		 * The meta object literal for the '<em><b>Semesters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STUDY_PROGRAMME__SEMESTERS = eINSTANCE.getStudyProgramme_Semesters();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STUDY_PROGRAMME__NAME = eINSTANCE.getStudyProgramme_Name();

		/**
		 * The meta object literal for the '{@link tdt4250.sp.Specialization <em>Specialization</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.sp.Specialization
		 * @see tdt4250.sp.impl.SpPackageImpl#getSpecialization()
		 * @generated
		 */
		EEnum SPECIALIZATION = eINSTANCE.getSpecialization();

		/**
		 * The meta object literal for the '{@link tdt4250.sp.Term <em>Term</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.sp.Term
		 * @see tdt4250.sp.impl.SpPackageImpl#getTerm()
		 * @generated
		 */
		EEnum TERM = eINSTANCE.getTerm();

		/**
		 * The meta object literal for the '{@link tdt4250.sp.CourseLevel <em>Course Level</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.sp.CourseLevel
		 * @see tdt4250.sp.impl.SpPackageImpl#getCourseLevel()
		 * @generated
		 */
		EEnum COURSE_LEVEL = eINSTANCE.getCourseLevel();

		/**
		 * The meta object literal for the '{@link tdt4250.sp.Status <em>Status</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.sp.Status
		 * @see tdt4250.sp.impl.SpPackageImpl#getStatus()
		 * @generated
		 */
		EEnum STATUS = eINSTANCE.getStatus();

	}

} //SpPackage
