/**
 */
package tdt4250.sp;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Specialization</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see tdt4250.sp.SpPackage#getSpecialization()
 * @model
 * @generated
 */
public enum Specialization implements Enumerator {
	/**
	 * The '<em><b>Software</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SOFTWARE_VALUE
	 * @generated
	 * @ordered
	 */
	SOFTWARE(0, "Software", "Software"),

	/**
	 * The '<em><b>Interaction Design</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTERACTION_DESIGN_VALUE
	 * @generated
	 * @ordered
	 */
	INTERACTION_DESIGN(1, "InteractionDesign", "Interaction Design, Game and Learning Technology"),

	/**
	 * The '<em><b>DB And S</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DB_AND_S_VALUE
	 * @generated
	 * @ordered
	 */
	DB_AND_S(2, "DBAndS", "Databases and Search"),

	/**
	 * The '<em><b>AI</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AI_VALUE
	 * @generated
	 * @ordered
	 */
	AI(3, "AI", "Artificial Intelligence");

	/**
	 * The '<em><b>Software</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SOFTWARE
	 * @model name="Software"
	 * @generated
	 * @ordered
	 */
	public static final int SOFTWARE_VALUE = 0;

	/**
	 * The '<em><b>Interaction Design</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTERACTION_DESIGN
	 * @model name="InteractionDesign" literal="Interaction Design, Game and Learning Technology"
	 * @generated
	 * @ordered
	 */
	public static final int INTERACTION_DESIGN_VALUE = 1;

	/**
	 * The '<em><b>DB And S</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DB_AND_S
	 * @model name="DBAndS" literal="Databases and Search"
	 * @generated
	 * @ordered
	 */
	public static final int DB_AND_S_VALUE = 2;

	/**
	 * The '<em><b>AI</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AI
	 * @model literal="Artificial Intelligence"
	 * @generated
	 * @ordered
	 */
	public static final int AI_VALUE = 3;

	/**
	 * An array of all the '<em><b>Specialization</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final Specialization[] VALUES_ARRAY =
		new Specialization[] {
			SOFTWARE,
			INTERACTION_DESIGN,
			DB_AND_S,
			AI,
		};

	/**
	 * A public read-only list of all the '<em><b>Specialization</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<Specialization> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Specialization</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static Specialization get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			Specialization result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Specialization</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static Specialization getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			Specialization result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Specialization</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static Specialization get(int value) {
		switch (value) {
			case SOFTWARE_VALUE: return SOFTWARE;
			case INTERACTION_DESIGN_VALUE: return INTERACTION_DESIGN;
			case DB_AND_S_VALUE: return DB_AND_S;
			case AI_VALUE: return AI;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private Specialization(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //Specialization
