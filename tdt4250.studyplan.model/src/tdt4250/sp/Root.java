/**
 */
package tdt4250.sp;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>root</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.sp.Root#getStudyPlan <em>Study Plan</em>}</li>
 *   <li>{@link tdt4250.sp.Root#getDepartment <em>Department</em>}</li>
 * </ul>
 *
 * @see tdt4250.sp.SpPackage#getRoot()
 * @model
 * @generated
 */
public interface Root extends EObject {
	/**
	 * Returns the value of the '<em><b>Study Plan</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Study Plan</em>' containment reference.
	 * @see #setStudyPlan(StudyPlan)
	 * @see tdt4250.sp.SpPackage#getRoot_StudyPlan()
	 * @model containment="true"
	 * @generated
	 */
	StudyPlan getStudyPlan();

	/**
	 * Sets the value of the '{@link tdt4250.sp.Root#getStudyPlan <em>Study Plan</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Study Plan</em>' containment reference.
	 * @see #getStudyPlan()
	 * @generated
	 */
	void setStudyPlan(StudyPlan value);

	/**
	 * Returns the value of the '<em><b>Department</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Department</em>' containment reference.
	 * @see #setDepartment(Department)
	 * @see tdt4250.sp.SpPackage#getRoot_Department()
	 * @model containment="true"
	 * @generated
	 */
	Department getDepartment();

	/**
	 * Sets the value of the '{@link tdt4250.sp.Root#getDepartment <em>Department</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Department</em>' containment reference.
	 * @see #getDepartment()
	 * @generated
	 */
	void setDepartment(Department value);

} // root
