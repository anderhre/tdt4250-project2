/**
 */
package tdt4250.sp.impl;

import java.lang.reflect.InvocationTargetException;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import tdt4250.sp.Course;
import tdt4250.sp.ElectiveCourseGroup;
import tdt4250.sp.MandatoryCourseGroup;
import tdt4250.sp.Semester;
import tdt4250.sp.SpFactory;
import tdt4250.sp.SpPackage;
import tdt4250.sp.Specialization;
import tdt4250.sp.Status;
import tdt4250.sp.Term;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Semester</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.sp.impl.SemesterImpl#getYear <em>Year</em>}</li>
 *   <li>{@link tdt4250.sp.impl.SemesterImpl#getTerm <em>Term</em>}</li>
 *   <li>{@link tdt4250.sp.impl.SemesterImpl#getSpecialization <em>Specialization</em>}</li>
 *   <li>{@link tdt4250.sp.impl.SemesterImpl#getMandatoryCourseGroup <em>Mandatory Course Group</em>}</li>
 *   <li>{@link tdt4250.sp.impl.SemesterImpl#getElectiveCourseGroup <em>Elective Course Group</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SemesterImpl extends MinimalEObjectImpl.Container implements Semester {
	/**
	 * The default value of the '{@link #getYear() <em>Year</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getYear()
	 * @generated
	 * @ordered
	 */
	protected static final int YEAR_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getYear() <em>Year</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getYear()
	 * @generated
	 * @ordered
	 */
	protected int year = YEAR_EDEFAULT;

	/**
	 * The default value of the '{@link #getTerm() <em>Term</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTerm()
	 * @generated
	 * @ordered
	 */
	protected static final Term TERM_EDEFAULT = Term.FALL;

	/**
	 * The cached value of the '{@link #getTerm() <em>Term</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTerm()
	 * @generated
	 * @ordered
	 */
	protected Term term = TERM_EDEFAULT;

	/**
	 * The default value of the '{@link #getSpecialization() <em>Specialization</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecialization()
	 * @generated
	 * @ordered
	 */
	protected static final Specialization SPECIALIZATION_EDEFAULT = Specialization.SOFTWARE;

	/**
	 * The cached value of the '{@link #getSpecialization() <em>Specialization</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecialization()
	 * @generated
	 * @ordered
	 */
	protected Specialization specialization = SPECIALIZATION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getMandatoryCourseGroup() <em>Mandatory Course Group</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMandatoryCourseGroup()
	 * @generated
	 * @ordered
	 */
	protected MandatoryCourseGroup mandatoryCourseGroup;

	/**
	 * The cached value of the '{@link #getElectiveCourseGroup() <em>Elective Course Group</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElectiveCourseGroup()
	 * @generated
	 * @ordered
	 */
	protected ElectiveCourseGroup electiveCourseGroup;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SemesterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SpPackage.Literals.SEMESTER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getYear() {
		return year;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setYear(int newYear) {
		int oldYear = year;
		year = newYear;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpPackage.SEMESTER__YEAR, oldYear, year));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Term getTerm() {
		return term;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTerm(Term newTerm) {
		Term oldTerm = term;
		term = newTerm == null ? TERM_EDEFAULT : newTerm;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpPackage.SEMESTER__TERM, oldTerm, term));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Specialization getSpecialization() {
		return specialization;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSpecialization(Specialization newSpecialization) {
		Specialization oldSpecialization = specialization;
		specialization = newSpecialization == null ? SPECIALIZATION_EDEFAULT : newSpecialization;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpPackage.SEMESTER__SPECIALIZATION, oldSpecialization, specialization));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Course> getElectives() {
		if (this.electiveCourseGroup == null) {
			this.setElectiveCourseGroup(SpFactory.eINSTANCE.createElectiveCourseGroup());
		}
		
		EList<Course> electives = new EObjectResolvingEList<Course>(Course.class, this, SpPackage.COURSE);
		
		electives.addAll(this.electiveCourseGroup.getVa());
		electives.addAll(this.electiveCourseGroup.getVb());
		
		return electives;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public EList<Course> getMandatory() {
		if (this.mandatoryCourseGroup == null) {
			this.setMandatoryCourseGroup(SpFactory.eINSTANCE.createMandatoryCourseGroup());
		}

		EList<Course> mandatory = new EObjectResolvingEList<Course>(Course.class, this, SpPackage.COURSE);

		if (this.mandatoryCourseGroup.getO() != null) {
			mandatory.addAll(this.mandatoryCourseGroup.getO());
		}
		if (this.mandatoryCourseGroup.getM1a() != null) {
			mandatory.addAll(this.mandatoryCourseGroup.getM1a());
		}
		if (this.mandatoryCourseGroup.getM2a() != null) {
			mandatory.addAll(this.mandatoryCourseGroup.getM2a());
		}

		return mandatory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public MandatoryCourseGroup getMandatoryCourseGroup() {
		return mandatoryCourseGroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMandatoryCourseGroup(MandatoryCourseGroup newMandatoryCourseGroup, NotificationChain msgs) {
		MandatoryCourseGroup oldMandatoryCourseGroup = mandatoryCourseGroup;
		mandatoryCourseGroup = newMandatoryCourseGroup;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SpPackage.SEMESTER__MANDATORY_COURSE_GROUP, oldMandatoryCourseGroup, newMandatoryCourseGroup);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMandatoryCourseGroup(MandatoryCourseGroup newMandatoryCourseGroup) {
		if (newMandatoryCourseGroup != mandatoryCourseGroup) {
			NotificationChain msgs = null;
			if (mandatoryCourseGroup != null)
				msgs = ((InternalEObject)mandatoryCourseGroup).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SpPackage.SEMESTER__MANDATORY_COURSE_GROUP, null, msgs);
			if (newMandatoryCourseGroup != null)
				msgs = ((InternalEObject)newMandatoryCourseGroup).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SpPackage.SEMESTER__MANDATORY_COURSE_GROUP, null, msgs);
			msgs = basicSetMandatoryCourseGroup(newMandatoryCourseGroup, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpPackage.SEMESTER__MANDATORY_COURSE_GROUP, newMandatoryCourseGroup, newMandatoryCourseGroup));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ElectiveCourseGroup getElectiveCourseGroup() {
		return electiveCourseGroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetElectiveCourseGroup(ElectiveCourseGroup newElectiveCourseGroup, NotificationChain msgs) {
		ElectiveCourseGroup oldElectiveCourseGroup = electiveCourseGroup;
		electiveCourseGroup = newElectiveCourseGroup;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SpPackage.SEMESTER__ELECTIVE_COURSE_GROUP, oldElectiveCourseGroup, newElectiveCourseGroup);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setElectiveCourseGroup(ElectiveCourseGroup newElectiveCourseGroup) {
		if (newElectiveCourseGroup != electiveCourseGroup) {
			NotificationChain msgs = null;
			if (electiveCourseGroup != null)
				msgs = ((InternalEObject)electiveCourseGroup).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SpPackage.SEMESTER__ELECTIVE_COURSE_GROUP, null, msgs);
			if (newElectiveCourseGroup != null)
				msgs = ((InternalEObject)newElectiveCourseGroup).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SpPackage.SEMESTER__ELECTIVE_COURSE_GROUP, null, msgs);
			msgs = basicSetElectiveCourseGroup(newElectiveCourseGroup, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpPackage.SEMESTER__ELECTIVE_COURSE_GROUP, newElectiveCourseGroup, newElectiveCourseGroup));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public void addCourse(Course course, Status status) {
		if (this.mandatoryCourseGroup == null) {
			this.setMandatoryCourseGroup(SpFactory.eINSTANCE.createMandatoryCourseGroup());
		}
		if (this.electiveCourseGroup == null) {
			this.setElectiveCourseGroup(SpFactory.eINSTANCE.createElectiveCourseGroup());
		}
		if (course != null) {
			switch (status) {
				case O: this.mandatoryCourseGroup.getO().add(course);
				case M1A: this.mandatoryCourseGroup.getM1a().add(course);
				case M2A: this.mandatoryCourseGroup.getM2a().add(course);
				case VA: this.electiveCourseGroup.getVa().add(course);
				case VB: this.electiveCourseGroup.getVb().add(course);
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public void chooseSpecialization(Specialization specialization) {
		this.setSpecialization(specialization);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SpPackage.SEMESTER__MANDATORY_COURSE_GROUP:
				return basicSetMandatoryCourseGroup(null, msgs);
			case SpPackage.SEMESTER__ELECTIVE_COURSE_GROUP:
				return basicSetElectiveCourseGroup(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SpPackage.SEMESTER__YEAR:
				return getYear();
			case SpPackage.SEMESTER__TERM:
				return getTerm();
			case SpPackage.SEMESTER__SPECIALIZATION:
				return getSpecialization();
			case SpPackage.SEMESTER__MANDATORY_COURSE_GROUP:
				return getMandatoryCourseGroup();
			case SpPackage.SEMESTER__ELECTIVE_COURSE_GROUP:
				return getElectiveCourseGroup();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SpPackage.SEMESTER__YEAR:
				setYear((Integer)newValue);
				return;
			case SpPackage.SEMESTER__TERM:
				setTerm((Term)newValue);
				return;
			case SpPackage.SEMESTER__SPECIALIZATION:
				setSpecialization((Specialization)newValue);
				return;
			case SpPackage.SEMESTER__MANDATORY_COURSE_GROUP:
				setMandatoryCourseGroup((MandatoryCourseGroup)newValue);
				return;
			case SpPackage.SEMESTER__ELECTIVE_COURSE_GROUP:
				setElectiveCourseGroup((ElectiveCourseGroup)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SpPackage.SEMESTER__YEAR:
				setYear(YEAR_EDEFAULT);
				return;
			case SpPackage.SEMESTER__TERM:
				setTerm(TERM_EDEFAULT);
				return;
			case SpPackage.SEMESTER__SPECIALIZATION:
				setSpecialization(SPECIALIZATION_EDEFAULT);
				return;
			case SpPackage.SEMESTER__MANDATORY_COURSE_GROUP:
				setMandatoryCourseGroup((MandatoryCourseGroup)null);
				return;
			case SpPackage.SEMESTER__ELECTIVE_COURSE_GROUP:
				setElectiveCourseGroup((ElectiveCourseGroup)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SpPackage.SEMESTER__YEAR:
				return year != YEAR_EDEFAULT;
			case SpPackage.SEMESTER__TERM:
				return term != TERM_EDEFAULT;
			case SpPackage.SEMESTER__SPECIALIZATION:
				return specialization != SPECIALIZATION_EDEFAULT;
			case SpPackage.SEMESTER__MANDATORY_COURSE_GROUP:
				return mandatoryCourseGroup != null;
			case SpPackage.SEMESTER__ELECTIVE_COURSE_GROUP:
				return electiveCourseGroup != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case SpPackage.SEMESTER___ADD_COURSE__COURSE_STATUS:
				addCourse((Course)arguments.get(0), (Status)arguments.get(1));
				return null;
			case SpPackage.SEMESTER___CHOOSE_SPECIALIZATION__SPECIALIZATION:
				chooseSpecialization((Specialization)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (year: ");
		result.append(year);
		result.append(", term: ");
		result.append(term);
		result.append(", specialization: ");
		result.append(specialization);
		result.append(')');
		return result.toString();
	}

} //SemesterImpl
