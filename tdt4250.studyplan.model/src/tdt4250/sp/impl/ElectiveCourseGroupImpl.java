/**
 */
package tdt4250.sp.impl;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import tdt4250.sp.Course;
import tdt4250.sp.ElectiveCourseGroup;
import tdt4250.sp.SpPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Elective Course Group</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.sp.impl.ElectiveCourseGroupImpl#getVa <em>Va</em>}</li>
 *   <li>{@link tdt4250.sp.impl.ElectiveCourseGroupImpl#getVb <em>Vb</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ElectiveCourseGroupImpl extends MinimalEObjectImpl.Container implements ElectiveCourseGroup {
	/**
	 * The cached value of the '{@link #getVa() <em>Va</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVa()
	 * @generated
	 * @ordered
	 */
	protected EList<Course> va;

	/**
	 * The cached value of the '{@link #getVb() <em>Vb</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVb()
	 * @generated
	 * @ordered
	 */
	protected EList<Course> vb;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ElectiveCourseGroupImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SpPackage.Literals.ELECTIVE_COURSE_GROUP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Course> getVa() {
		if (va == null) {
			va = new EObjectResolvingEList<Course>(Course.class, this, SpPackage.ELECTIVE_COURSE_GROUP__VA);
		}
		return va;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Course> getVb() {
		if (vb == null) {
			vb = new EObjectResolvingEList<Course>(Course.class, this, SpPackage.ELECTIVE_COURSE_GROUP__VB);
		}
		return vb;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SpPackage.ELECTIVE_COURSE_GROUP__VA:
				return getVa();
			case SpPackage.ELECTIVE_COURSE_GROUP__VB:
				return getVb();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SpPackage.ELECTIVE_COURSE_GROUP__VA:
				getVa().clear();
				getVa().addAll((Collection<? extends Course>)newValue);
				return;
			case SpPackage.ELECTIVE_COURSE_GROUP__VB:
				getVb().clear();
				getVb().addAll((Collection<? extends Course>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SpPackage.ELECTIVE_COURSE_GROUP__VA:
				getVa().clear();
				return;
			case SpPackage.ELECTIVE_COURSE_GROUP__VB:
				getVb().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SpPackage.ELECTIVE_COURSE_GROUP__VA:
				return va != null && !va.isEmpty();
			case SpPackage.ELECTIVE_COURSE_GROUP__VB:
				return vb != null && !vb.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ElectiveCourseGroupImpl
