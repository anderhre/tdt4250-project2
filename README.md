## General
The project contains a model to text (M2T) transformation, where an instance of the model (xmi) is transformed to HTML with use of Acceleo.

The project is configured as listed below:
* <b>Model</b>: ```/tdt4250.studyplan.model/model/studyPlan.xmi```
* <b>Target</b>: ```/org.eclipse.acceleo.module.project2/src/org/eclipse/acceleo/module/project2/main```

To change model or target, click ```Run Configurations...``` (in the "run" dropdown) and edit the paths in the pop-up.

To run the transformation, click ```Generate``` (in the "run" dropdown).

## Changes since project 1
### Model changes
* Replaced the derived feature "noOfCourses" with "abbreviation", which is based on the Department "name".
Also created new test for abbreviation.

### Model-instance changes
* Added a lot of courses and semesters for all specializations.
XMI now contains a (almost) complete first year of the study plan for MSc in Informatics.

### Tests
The project still contains generated test code and four manually written tests from project 1:
* ```testGetAbbreviation``` and ```testAddCourse__Course``` in ```DepartmentTest.java```
* ```testAddCourse__Course__Status``` and ```testChooseSpecialization__Specialization``` in ```SemesterTest.java```