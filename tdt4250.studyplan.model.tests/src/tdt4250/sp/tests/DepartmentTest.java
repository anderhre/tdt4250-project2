/**
 */
package tdt4250.sp.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;
import tdt4250.sp.Course;
import tdt4250.sp.Department;
import tdt4250.sp.SpFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Department</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are tested:
 * <ul>
 *   <li>{@link tdt4250.sp.Department#getAbbreviation() <em>Abbreviation</em>}</li>
 * </ul>
 * </p>
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link tdt4250.sp.Department#addCourse(tdt4250.sp.Course) <em>Add Course</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class DepartmentTest extends TestCase {

	/**
	 * The fixture for this Department test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Department fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(DepartmentTest.class);
	}

	/**
	 * Constructs a new Department test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DepartmentTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Department test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(Department fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Department test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Department getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(SpFactory.eINSTANCE.createDepartment());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link tdt4250.sp.Department#getAbbreviation() <em>Abbreviation</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.sp.Department#getAbbreviation()
	 * @generated NOT
	 */
	public void testGetAbbreviation() {
		Department dep = SpFactory.eINSTANCE.createDepartment();
		dep.setName("Institutt for Datateknologi og Informatikk");
		
		// Assert abbreviation is "IDI"
		assertEquals(dep.getAbbreviation(), "IDI");
	}

	/**
	 * Tests the '{@link tdt4250.sp.Department#addCourse(tdt4250.sp.Course) <em>Add Course</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.sp.Department#addCourse(tdt4250.sp.Course)
	 * @generated NOT
	 */
	public void testAddCourse__Course() {
		// Create department
		Department dep = SpFactory.eINSTANCE.createDepartment();
		// Assert department has no courses
		assertEquals(dep.getCourses().size(), 0);
		// Create a course
		Course course = SpFactory.eINSTANCE.createCourse();
		course.setName("Advanced Software Engineering");
		// Add course to department
		dep.addCourse(course);
		// Assert course was added correctly
		assertEquals(dep.getCourses().size(), 1);
		assertEquals(dep.getCourses().get(0).getName(), "Advanced Software Engineering");
	}

} //DepartmentTest
