/**
 */
package tdt4250.sp.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import tdt4250.sp.MandatoryCourseGroup;
import tdt4250.sp.SpFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Mandatory Course Group</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class MandatoryCourseGroupTest extends TestCase {

	/**
	 * The fixture for this Mandatory Course Group test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MandatoryCourseGroup fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(MandatoryCourseGroupTest.class);
	}

	/**
	 * Constructs a new Mandatory Course Group test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MandatoryCourseGroupTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Mandatory Course Group test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(MandatoryCourseGroup fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Mandatory Course Group test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MandatoryCourseGroup getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(SpFactory.eINSTANCE.createMandatoryCourseGroup());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //MandatoryCourseGroupTest
