/**
 */
package tdt4250.sp.tests;
import tdt4250.sp.Specialization;

import junit.framework.TestCase;

import junit.textui.TestRunner;
import tdt4250.sp.Course;
import tdt4250.sp.Department;
import tdt4250.sp.Semester;
import tdt4250.sp.SpFactory;
import tdt4250.sp.Status;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Semester</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link tdt4250.sp.Semester#addCourse(tdt4250.sp.Course, tdt4250.sp.Status) <em>Add Course</em>}</li>
 *   <li>{@link tdt4250.sp.Semester#chooseSpecialization(tdt4250.sp.Specialization) <em>Choose Specialization</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class SemesterTest extends TestCase {

	/**
	 * The fixture for this Semester test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Semester fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(SemesterTest.class);
	}

	/**
	 * Constructs a new Semester test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SemesterTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Semester test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(Semester fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Semester test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Semester getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(SpFactory.eINSTANCE.createSemester());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link tdt4250.sp.Semester#addCourse(tdt4250.sp.Course, tdt4250.sp.Status) <em>Add Course</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.sp.Semester#addCourse(tdt4250.sp.Course, tdt4250.sp.Status)
	 * @generated NOT
	 */
	public void testAddCourse__Course_Status() {
		// Create department
		Department dep = SpFactory.eINSTANCE.createDepartment();
		// Create course and add it to department
		Course course = SpFactory.eINSTANCE.createCourse();
		dep.addCourse(course);
		// Create semester
		Semester semester = SpFactory.eINSTANCE.createSemester();
		
		// Assert semester has no mandatory courses
		assertEquals(semester.getMandatory().size(), 0);

		// Add course to semester as mandatory course (Status.O)
		semester.addCourse(course, Status.O);
		// Assert semester now has one mandatory course
		assertEquals(semester.getMandatory().size(), 1);
		assertEquals(semester.getMandatoryCourseGroup().getO().size(), 1);
		
		// Add course to semester as an elective (Status.VA)
		semester.addCourse(course, Status.VA);
		// Assert semester now has one elective course
		assertEquals(semester.getElectives().size(), 1);
		assertEquals(semester.getElectiveCourseGroup().getVa().size(), 1);
	}

	/**
	 * Tests the '{@link tdt4250.sp.Semester#chooseSpecialization(tdt4250.sp.Specialization) <em>Choose Specialization</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.sp.Semester#chooseSpecialization(tdt4250.sp.Specialization)
	 * @generated NOT
	 */
	public void testChooseSpecialization__Specialization() {
		// Create semester
		Semester semester = SpFactory.eINSTANCE.createSemester();
		// Set specialization
		semester.setSpecialization(Specialization.SOFTWARE);
		assertEquals(semester.getSpecialization(), Specialization.SOFTWARE);
	}

} //SemesterTest
