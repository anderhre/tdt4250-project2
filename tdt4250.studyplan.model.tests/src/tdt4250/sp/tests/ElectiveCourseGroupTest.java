/**
 */
package tdt4250.sp.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import tdt4250.sp.ElectiveCourseGroup;
import tdt4250.sp.SpFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Elective Course Group</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class ElectiveCourseGroupTest extends TestCase {

	/**
	 * The fixture for this Elective Course Group test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ElectiveCourseGroup fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ElectiveCourseGroupTest.class);
	}

	/**
	 * Constructs a new Elective Course Group test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElectiveCourseGroupTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Elective Course Group test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(ElectiveCourseGroup fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Elective Course Group test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ElectiveCourseGroup getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(SpFactory.eINSTANCE.createElectiveCourseGroup());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //ElectiveCourseGroupTest
